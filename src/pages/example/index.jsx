import { Helmet } from 'react-helmet';
import { Footer, NotFound, Unauthorized, LoggedInIcon } from '@components';

function Example() {
  return (
    <>
      <Helmet>
        <title>Example</title>
      </Helmet>
      <NotFound description="Halaman tidak ditemukan" />
      <Unauthorized description="Anda tidak memiliki akses" />
      <LoggedInIcon />
      <Footer></Footer>
    </>
  );
}

export default Example;
