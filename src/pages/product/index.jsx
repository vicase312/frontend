import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Box, ProductDetail, ProductDescription, UserCard, ProductSlider } from '@components';
import { getProductBySlug } from '@redux/reducers/product';
import { getUser } from '@redux/reducers/auth';
import { getOfferStatusByProdustAndUser } from '@redux/reducers/offer';
import { getStatusLike } from '@redux/reducers/product_like';

function Product() {
  const dispatch = useDispatch();
  const { slug } = useParams();
  const { data: product, loading: productLoading } = useSelector(
    (state) => state.product.productBySlug
  );
  const { data: offerStatus } = useSelector((state) => state.offer.offerStatus);
  const { data: user } = useSelector((state) => state.auth.user);

  useEffect(() => {
    if (slug) {
      dispatch(getProductBySlug(slug));
      dispatch(getUser());
    }
  }, [slug]); // eslint-disable-line

  useEffect(() => {
    if (product) {
      dispatch(getOfferStatusByProdustAndUser(product?.id));
      dispatch(getStatusLike(product?.id));
    }
  }, [product]); // eslint-disable-line

  return (
    <>
      <Box className="product-page">
        <Box className="container">
          <Box className="product-page-grid">
            <Box className="product-page-grid-left">
              <ProductSlider loading={productLoading} product={product ? product : []} />
              <Box className="block z-10 md:hidden relative">
                <ProductDetail
                  loading={productLoading}
                  canBuy={offerStatus === 0}
                  user={user}
                  product={product ? product : []}
                />
                <UserCard loading={productLoading} user={product?.user}></UserCard>
              </Box>
              <ProductDescription loading={productLoading} product={product ? product : []} />
            </Box>
            <Box className="product-page-grid-right hidden md:block">
              <ProductDetail
                loading={productLoading}
                canBuy={offerStatus === 0}
                user={user}
                product={product ? product : []}
              />
              <UserCard loading={productLoading} user={product?.user}></UserCard>
            </Box>
          </Box>
        </Box>
      </Box>
      <Helmet>
        <title>{product?.name}</title>
      </Helmet>
    </>
  );
}

export default Product;
