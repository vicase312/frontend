import { Helmet } from 'react-helmet';
import { Button, Form, Input, Select, Row, Col } from 'antd';
import { FiPlus, FiLoader, FiTrash } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Box, BackNavigation } from '@components';
import { useNavigate } from 'react-router-dom';

import { getCategories } from '@redux/reducers/category';
import { updateProduct, getProductByID, reset } from '@redux/reducers/product';
import { useUpload } from '@libs/upload';

const { TextArea } = Input;
const { Option } = Select;

const EditProduct = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { handleUpload } = useUpload();
  const { id: productID } = useParams();
  const [images, setImages] = useState([]);
  const [imageLoading, setImageLoading] = useState(false);
  const [status, setStatus] = useState(0);

  const { data: categories } = useSelector((state) => state.category.category);
  const { data: product } = useSelector((state) => state.product.productByID);
  const { loading: updateProductLoading, success: successUpdateProduct } = useSelector(
    (state) => state.product.updateProduct
  );
  useEffect(() => {
    dispatch(getCategories());
    dispatch(getProductByID(productID));
  }, [dispatch]); //eslint-disable-line

  useEffect(() => {
    if (product?.images) {
      let arr = [];
      product.images.map((image) => arr.push(image.url));
      setImages(arr);
    }
  }, [product]);

  const onFinish = async (values) => {
    setTimeout(() => {
      dispatch(
        updateProduct({
          id: productID,
          data: {
            ...values,
            status,
            images
          }
        })
      );
    }, 500);
  };

  const formInitialValues = {
    name: product?.name,
    price: product?.price,
    category_id: product?.category_id,
    description: product?.description
  };

  const handleUploadImage = async (e) => {
    e.persist();
    setImageLoading(true);
    const { data: result } = await handleUpload(e, 'products');
    let img = images.concat(result.file);
    setImageLoading(false);
    setImages(img);
  };

  const handleDeleteImage = (url) => {
    let img = images.filter((x) => x !== url);
    setImages(img);
  };

  useEffect(() => {
    if (successUpdateProduct) {
      dispatch(reset());
      navigate(`/${product.slug}`);
    }
  }, [successUpdateProduct]); //eslint-disable-line

  return (
    <>
      <Box className="product container">
        <BackNavigation className="profile-back" />
        {formInitialValues?.name && (
          <Form
            form={form}
            layout="vertical"
            autoComplete="off"
            onFinish={onFinish}
            initialValues={formInitialValues}
          >
            <Form.Item
              name="name"
              label="Nama Produk"
              rules={[
                {
                  required: true,
                  message: 'Nama produk harus diisi'
                }
              ]}
            >
              <Input placeholder="Nama Produk" />
            </Form.Item>
            <Form.Item
              name="price"
              label="Harga Produk"
              rules={[
                {
                  required: true,
                  message: 'Harga produk harus diisi'
                }
              ]}
            >
              <Input type="number" placeholder="Rp 0,00" />
            </Form.Item>
            <Form.Item
              name="category_id"
              label="Kategori"
              rules={[
                {
                  required: true,
                  message: 'Kategori produk harus diisi'
                }
              ]}
            >
              <Select placeholder="Pilih Kategori">
                {categories?.map((category) => (
                  <Option value={category?.id}>{category?.name}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="description"
              label="Deskripsi"
              rules={[
                {
                  required: true,
                  message: 'Deskripsi produk harus diisi'
                }
              ]}
            >
              <TextArea rows={2} placeholder="Contoh: Jalan Ikan Hiu 33" />
            </Form.Item>

            <Form.Item label="Foto Produk">
              <Box className="flex">
                {images?.length < 4 && (
                  <label htmlFor="uploader">
                    <Box to="#" className="add-image">
                      <Box className="add-image-icon">
                        {imageLoading ? (
                          <FiLoader size={22} className="animate-spin" />
                        ) : (
                          <FiPlus size={22} />
                        )}
                      </Box>
                    </Box>
                    <input
                      id="uploader"
                      style={{
                        visibility: 'hidden',
                        width: 0,
                        height: 0
                      }}
                      type="file"
                      onChange={handleUploadImage}
                    />
                  </label>
                )}
                {images?.map((image) => (
                  <Box className="relative">
                    <img src={image} className="add-image-result" alt="" />
                    <FiTrash
                      onClick={() => {
                        handleDeleteImage(image);
                      }}
                      className="absolute right-2 top-2 text-red-800 cursor-pointer"
                    />
                  </Box>
                ))}
              </Box>
            </Form.Item>

            <Form.Item>
              <Row gutter={16}>
                <Col span={12}>
                  <Button
                    type="secondary"
                    disabled={updateProductLoading}
                    loading={updateProductLoading}
                    htmlType="submit"
                    block
                    size="large"
                    onClick={() => setStatus(0)}
                  >
                    Preview
                  </Button>
                </Col>
                <Col span={12}>
                  <Button
                    type="primary"
                    disabled={updateProductLoading}
                    loading={updateProductLoading}
                    htmlType="submit"
                    block
                    size="large"
                    onClick={() => setStatus(1)}
                  >
                    Terbitkan
                  </Button>
                </Col>
              </Row>
            </Form.Item>
          </Form>
        )}
      </Box>
      <Helmet>
        <title>Edit Produk</title>
      </Helmet>
    </>
  );
};

export default EditProduct;
