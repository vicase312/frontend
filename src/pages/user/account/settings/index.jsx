import { Helmet } from 'react-helmet';
import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Box, BackNavigation } from '@components';
import { updatePassword } from '@redux/reducers/auth';

const AccountSettings = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { loading: updateUserLoading } = useSelector((state) => state.auth.updatePassword);

  const onFinish = async (values) => {
    dispatch(updatePassword(values));
  };
  return (
    <>
      <Box className="profile container">
        <BackNavigation className="profile-back" />
        <Form form={form} layout="vertical" autoComplete="off" onFinish={onFinish}>
          <Form.Item
            name="oldPassword"
            label="Password saat ini"
            rules={[{ required: true, message: 'Password saat ini harus diisi' }]}
          >
            <Input.Password placeholder="Password saat ini" />
          </Form.Item>
          <Form.Item
            name="newPassword"
            label="Password baru"
            rules={[
              { required: true, message: 'Password baru harus diisi' },
              { min: 8, message: 'Password minimal 8 karakter' }
            ]}
          >
            <Input.Password placeholder="Password baru" />
          </Form.Item>
          <Form.Item
            name="confirmNewPassword"
            label="Konfirmasi password baru"
            rules={[
              { required: true, message: 'Konfirmasi password baru harus diisi' },
              { min: 8, message: 'Password minimal 8 karakter' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('newPassword') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Konfirmasi password baru tidak sama'));
                }
              })
            ]}
          >
            <Input.Password placeholder="Konfirmasi password baru" />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              block
              size="large"
              disable={updateUserLoading}
              loading={updateUserLoading}
              htmlType="submit"
            >
              Simpan
            </Button>
          </Form.Item>
        </Form>
      </Box>
      <Helmet>
        <title>Pengaturan Akun</title>
      </Helmet>
    </>
  );
};

export default AccountSettings;
