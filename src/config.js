const config = {
  API_URL: process.env.REACT_APP_API_HOST || 'https://api-secondhand.herokuapp.com',
  API_VERSION: 'v1'
};

export default config;
