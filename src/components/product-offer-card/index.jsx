import { Box } from '@components';
import { Avatar, Card, Typography, Button, Skeleton, Badge } from 'antd';
import dateFormat from 'dateformat';
import { useNavigate } from 'react-router-dom';
import { BsWhatsapp } from 'react-icons/bs';
import { formatRupiah } from '@libs/number';
import { offerStatusParser } from '@libs/mapper';
const { Meta } = Card;
const { Text } = Typography;

function ProductOfferCard({
  offer,
  loading,
  showAction = false,
  rejectOffer,
  rejectOfferLoading,
  acceptOffer,
  acceptOfferLoading,
  openModalStatus
}) {
  const navigate = useNavigate();
  const status = offerStatusParser(offer?.status);

  return loading ? (
    <Skeleton.Button className="skeleton-user-card" active />
  ) : (
    <Box className="product-offer-div">
      <Card onClick={() => navigate(`/user/offer/${offer.id}`, { replace: true })}>
        <Meta
          avatar={
            <Avatar
              className="product-offer-img"
              src={offer?.product?.images[0]?.url || 'https://picsum.photos/200/300'}
            />
          }
        />
        <Box className="product-offer-desc">
          <Box className="top">
            <Text type="secondary">Penawaran produk</Text>
            <Text type="secondary">
              {dateFormat(offer?.created_at, 'dd mmm, HH:MM')} <br />
            </Text>
          </Box>
          <Box className="middle">
            <Text>{offer?.product?.name}</Text>
            <Text>{formatRupiah(offer?.product?.price)}</Text>
            <Text>Ditawar {formatRupiah(offer?.offer_price)}</Text>
            <Box className="inline-block mt-1">
              <Badge count={status?.name} style={{ backgroundColor: status?.color }} />
            </Box>
          </Box>
          {showAction && offer?.status === 1 && (
            <Box className="bottom">
              <Button
                type="primary"
                loading={rejectOfferLoading}
                onClick={() => rejectOffer()}
                className="button-offer"
                ghost
                shape="round"
                size="large"
              >
                Tolak
              </Button>
              <Button
                loading={acceptOfferLoading}
                onClick={() => acceptOffer()}
                type="primary"
                className="button-offer"
                shape="round"
                size="large"
              >
                Terima
              </Button>
            </Box>
          )}
          {showAction && offer?.status === 2 && (
            <Box className="bottom">
              <Button
                type="primary"
                // loading={rejectOfferLoading}
                onClick={() => openModalStatus()}
                className="button-offer"
                ghost
                shape="round"
                size="large"
              >
                Status
              </Button>
              <a href={`https://wa.me/${offer?.user?.phone}`} target="_blank">
                <Button type="primary" className="button-offer" shape="round" size="large">
                  Hubungi di <BsWhatsapp className="inline-block ml-2" />
                </Button>
              </a>
            </Box>
          )}
        </Box>
      </Card>
    </Box>
  );
}

export default ProductOfferCard;
