import { Box } from '@components';
import { ReactComponent as NotFoundImg } from '@assets/svg/notfound.svg';

const NotFound = ({ description }) => {
  return (
    <Box className="not-found  ">
      <NotFoundImg className="not-found-image" />

      <Box className="not-found-text">{description}</Box>
    </Box>
  );
};

export default NotFound;
