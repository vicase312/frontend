import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';

export const getCategories = createAsyncThunk('categories', async () => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/categories`);
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  category: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // CATEGORY
    [getCategories.pending]: (state) => {
      state.category.loading = true;
    },
    [getCategories.fulfilled]: (state, action) => {
      state.category.data = action.payload.data.data;
      state.category.success = true;
      state.category.error = false;
      state.category.errorMessage = null;
      state.category.loading = false;
    },
    [getCategories.rejected]: (state, action) => {
      state.category.success = false;
      state.category.error = action.error.message;
      state.category.errorMessage = action.payload.message;
      state.category.loading = false;
    }
  }
});

export const { reset } = categorySlice.actions;

export default categorySlice.reducer;
