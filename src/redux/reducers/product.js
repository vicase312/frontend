import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import useToast from '@libs/toast';
import { getToken } from './auth';

export const getProducts = createAsyncThunk('products', async (values) => {
  try {
    const page = values?.page || '1';
    const pageSize = values?.page_size || '12';
    const category_id = values?.category_id || '';
    const name = values?.name || '';

    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/products?name=${name}&category_id=${category_id}&page=${page}&page_size=${pageSize}`
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductsByName = createAsyncThunk('products/name', async (values) => {
  try {
    const page = values?.page || '1';
    const pageSize = values?.page_size || '12';
    const name = values?.name || '';

    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/products?name=${name}&page=${page}&page_size=${pageSize}`
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductsByOwner = createAsyncThunk('user/products', async () => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/user/products`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductsByOwnerSold = createAsyncThunk('user/products/sold', async () => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/user/products-sold`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductByID = createAsyncThunk('user/product', async (id) => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/user/product/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductBySlug = createAsyncThunk('productBySlug', async (slug) => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/product/${slug}`);
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const createProduct = createAsyncThunk('createProduct', async (values) => {
  const toast = useToast();
  try {
    const response = await axios.post(`${config.API_URL}/${config.API_VERSION}/products`, values, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 201) {
      toast(response.data.message, 'success');
      console.log(response.data);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const updateProduct = createAsyncThunk('updateProduct', async (values) => {
  const toast = useToast();
  try {
    const response = await axios.put(
      `${config.API_URL}/${config.API_VERSION}/products/${values.id}`,
      values.data,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      toast('Success update data', 'success');
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  product: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    isSearch: false,
    data: [],
    pagination: []
  },
  productBySlug: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: null
  },
  productsByOwner: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  productsByOwnerSold: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  productByID: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  createProduct: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  updateProduct: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    [getProducts.pending]: (state) => {
      state.product.loading = true;
      state.product.isSearch = false;
    },
    [getProducts.fulfilled]: (state, action) => {
      state.product.data = action.payload.data.data;
      state.product.pagination = action.payload.data.meta;
      state.product.success = true;
      state.product.error = false;
      state.product.isSearch = false;
      state.product.errorMessage = null;
      state.product.loading = false;
    },
    [getProducts.rejected]: (state, action) => {
      state.product.success = false;
      state.product.error = action.error.message;
      state.product.errorMessage = action.payload.message;
      state.product.loading = false;
    },
    [getProductsByName.pending]: (state) => {
      state.product.loading = true;
      state.product.isSearch = true;
    },
    [getProductsByName.fulfilled]: (state, action) => {
      state.product.data = action.payload.data.data;
      state.product.pagination = action.payload.data.meta;
      state.product.success = true;
      state.product.error = false;
      state.product.isSearch = false;
      state.product.errorMessage = null;
      state.product.loading = false;
    },
    [getProductsByName.rejected]: (state, action) => {
      state.product.success = false;
      state.product.error = action.error.message;
      state.product.errorMessage = action.payload.message;
      state.product.loading = false;
    },
    [getProductsByOwner.pending]: (state) => {
      state.productsByOwner.loading = true;
    },
    [getProductsByOwner.fulfilled]: (state, action) => {
      state.productsByOwner.data = action.payload.data.data;
      state.productsByOwner.success = true;
      state.productsByOwner.error = false;
      state.productsByOwner.errorMessage = null;
      state.productsByOwner.loading = false;
    },
    [getProductsByOwner.rejected]: (state, action) => {
      state.productsByOwner.success = false;
      state.productsByOwner.error = action.error.message;
      state.productsByOwner.errorMessage = action.payload.message;
      state.productsByOwner.loading = false;
    },
    [getProductsByOwnerSold.pending]: (state) => {
      state.productsByOwnerSold.loading = true;
    },
    [getProductsByOwnerSold.fulfilled]: (state, action) => {
      state.productsByOwnerSold.data = action.payload.data.data;
      state.productsByOwnerSold.success = true;
      state.productsByOwnerSold.error = false;
      state.productsByOwnerSold.errorMessage = null;
      state.productsByOwnerSold.loading = false;
    },
    [getProductsByOwnerSold.rejected]: (state, action) => {
      state.productsByOwnerSold.success = false;
      state.productsByOwnerSold.error = action.error.message;
      state.productsByOwnerSold.errorMessage = action.payload.message;
      state.productsByOwnerSold.loading = false;
    },
    [getProductBySlug.pending]: (state) => {
      state.productBySlug.loading = true;
    },
    [getProductBySlug.fulfilled]: (state, action) => {
      state.productBySlug.data = action.payload.data.data;
      state.productBySlug.success = true;
      state.productBySlug.error = false;
      state.productBySlug.errorMessage = null;
      state.productBySlug.loading = false;
    },
    [getProductByID.rejected]: (state, action) => {
      state.productByID.success = false;
      state.productByID.error = action.error.message;
      state.productByID.errorMessage = action.payload.message;
      state.productByID.loading = false;
    },
    [getProductByID.pending]: (state) => {
      state.productByID.loading = true;
    },
    [getProductByID.fulfilled]: (state, action) => {
      state.productByID.data = action.payload.data.data;
      state.productByID.success = true;
      state.productByID.error = false;
      state.productByID.errorMessage = null;
      state.productByID.loading = false;
    },
    [getProductBySlug.rejected]: (state, action) => {
      state.productBySlug.success = false;
      state.productBySlug.error = action.error.message;
      state.productBySlug.errorMessage = action.payload.message;
      state.productBySlug.loading = false;
    },
    [createProduct.pending]: (state) => {
      state.createProduct.loading = true;
    },
    [createProduct.fulfilled]: (state, action) => {
      state.createProduct.data = action.payload.data.data;
      state.createProduct.success = true;
      state.createProduct.error = false;
      state.createProduct.errorMessage = null;
      state.createProduct.loading = false;
    },
    [createProduct.rejected]: (state, action) => {
      state.createProduct.success = false;
      state.createProduct.error = action.error.message;
      state.createProduct.errorMessage = action.payload.message;
      state.createProduct.loading = false;
    },
    [updateProduct.pending]: (state) => {
      state.updateProduct.loading = true;
    },
    [updateProduct.fulfilled]: (state, action) => {
      state.updateProduct.data = action.payload.data.data;
      state.updateProduct.success = true;
      state.updateProduct.error = false;
      state.updateProduct.errorMessage = null;
      state.updateProduct.loading = false;
    },
    [updateProduct.rejected]: (state, action) => {
      state.updateProduct.success = false;
      state.updateProduct.error = action.error.message;
      state.updateProduct.errorMessage = action.payload.message;
      state.updateProduct.loading = false;
    }
  }
});

export const { reset } = productSlice.actions;

export default productSlice.reducer;
