import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import { getToken } from './auth';

export const getNotifications = createAsyncThunk('getNotifications', async (values) => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/notifications?page=${values.page || 1}&page_size=5`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      console.log(response);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const updateRead = createAsyncThunk('updateRead', async (values) => {
  try {
    const response = await axios.put(
      `${config.API_URL}/${config.API_VERSION}/notification/${values.id}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 202) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  notifications: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    isSearch: false,
    data: [],
    pagination: []
  },
  updateRead: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    [getNotifications.pending]: (state) => {
      state.notifications.loading = true;
    },
    [getNotifications.fulfilled]: (state, action) => {
      state.notifications.data = action.payload.data.data;
      state.notifications.pagination = action.payload.data.meta;
      state.notifications.success = true;
      state.notifications.error = false;
      state.notifications.errorMessage = null;
      state.notifications.loading = false;
    },
    [getNotifications.rejected]: (state, action) => {
      state.notifications.success = false;
      state.notifications.error = action.error.message;
      state.notifications.errorMessage = action.payload.message;
      state.notifications.loading = false;
    },
    [updateRead.pending]: (state) => {
      state.updateRead.loading = true;
    },
    [updateRead.fulfilled]: (state, action) => {
      state.updateRead.success = true;
      state.updateRead.error = false;
      state.updateRead.errorMessage = null;
      state.updateRead.loading = false;
    },
    [updateRead.rejected]: (state, action) => {
      state.updateRead.success = false;
      state.updateRead.error = action.error.message;
      state.updateRead.errorMessage = action.payload.message;
      state.updateRead.loading = false;
    }
  }
});

export const { reset } = notificationSlice.actions;

export default notificationSlice.reducer;
