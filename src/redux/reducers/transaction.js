import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import { getToken } from './auth';

export const getTransactionBuyer = createAsyncThunk('getTransactionBuyer', async () => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/user/transaction/buyer`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getTransactionSeller = createAsyncThunk('getTransactionSeller', async () => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/user/transaction/seller`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  transactionBuyer: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  transactionSeller: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const transactionSlice = createSlice({
  name: 'transaction',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // TRANSACTION BUYER
    [getTransactionBuyer.pending]: (state) => {
      state.transactionBuyer.loading = true;
    },
    [getTransactionBuyer.fulfilled]: (state, action) => {
      state.transactionBuyer.data = action.payload.data.data;
      state.transactionBuyer.success = true;
      state.transactionBuyer.error = false;
      state.transactionBuyer.errorMessage = null;
      state.transactionBuyer.loading = false;
    },
    [getTransactionBuyer.rejected]: (state, action) => {
      state.transactionBuyer.success = false;
      state.transactionBuyer.error = action.error.message;
      state.transactionBuyer.errorMessage = action.payload.message;
      state.transactionBuyer.loading = false;
    },
    // TRANSACTION SELLER
    [getTransactionSeller.pending]: (state) => {
      state.transactionSeller.loading = true;
    },
    [getTransactionSeller.fulfilled]: (state, action) => {
      state.transactionSeller.data = action.payload.data.data;
      state.transactionSeller.success = true;
      state.transactionSeller.error = false;
      state.transactionSeller.errorMessage = null;
      state.transactionSeller.loading = false;
    },
    [getTransactionSeller.rejected]: (state, action) => {
      state.transactionSeller.success = false;
      state.transactionSeller.error = action.error.message;
      state.transactionSeller.errorMessage = action.payload.message;
      state.transactionSeller.loading = false;
    }
  }
});

export const { reset } = transactionSlice.actions;

export default transactionSlice.reducer;
